﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Utilities.Results
{
    public class ErroeResult : Result
    {
        public ErroeResult(string message) : base(false, message)
        {
        }
        public ErroeResult() : base(false)
        {
        }
    }
}
